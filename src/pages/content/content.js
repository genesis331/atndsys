import React, {Component} from 'react';
import 'antd/dist/antd.css';
import './content.css';
import {Card, Calendar, Statistic, Row, Col, Icon, Typography, Carousel, Tooltip, Tabs, Table, Divider, Form, Input, Button, Skeleton, Modal, message, TimePicker, DatePicker, Checkbox} from 'antd';
import Header from "../../components/header/header";
import {Link, Redirect} from "react-router-dom";
import cardlogo from '../../assets/credit-card.svg';
import moment from "moment";

const {Title, Text} = Typography;
const {TabPane} = Tabs;

let memberdata = [];
let activitydata = [];

class MemberAddFormComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleCardReg: false,
            CardRegNum: "",
            submitBtnStatus: true
        };
    }

    showModalCardReg = () => {
        this.setState({
            visibleCardReg: true
        });
    };

    detectCardRegChange = (value) => {
        if (value.length >= 10) {
            this.setState({
                visibleCardReg: false,
                CardRegNum: value,
                submitBtnStatus: false
            });
        }
    };

    handleOkCardReg = () => {
        this.setState({
            visibleCardReg: false,
        });
    };

    handleCancelCardReg = () => {
        this.setState({
            visibleCardReg: false,
            CardRegNum: "",
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch('https://atndsys-server.now.sh/addmember?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW') + '&class=' + values.class + '&schno=' + values.schno + '&name=' + values.name + '&cardnum=' + this.state.CardRegNum).then(response => response.text()).then(data => {
                    if (data === true || data === "true") {
                        message.info('New member added.');
                    }
                });
                this.props.form.resetFields();
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Modal
                    title="Card Registration"
                    visible={this.state.visibleCardReg}
                    onOk={this.handleOkCardReg}
                    onCancel={this.handleCancelCardReg}
                    footer={[
                        <Button key="cancel" type="normal" onClick={this.handleCancelMemberDelete}>
                            Cancel
                        </Button>
                    ]}
                    className="card-reg-modal"
                >
                    <div className="card-reg-modal-icon">
                        <img src={cardlogo} alt="Register Card"/>
                    </div>
                    <div className="card-reg-modal-indi">
                        <Text strong={true}>Tap card on scanner.</Text>
                    </div>
                    <div className="card-reg-modal-input">
                        <input type="text" autoFocus={true} onChange={(value) => {
                            this.detectCardRegChange(value.target.value);
                        }}/>
                    </div>
                </Modal>
                <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'The name of the member is required.' }],
                    })(
                        <Input
                            placeholder="Name"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('class', {
                        rules: [{ required: true, message: 'Invalid class.', min: 3, max: 4 }],
                    })(
                        <Input
                            placeholder="Class"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('schno', {
                        rules: [{ required: true, message: 'Invalid school number.', min: 5, max: 5 }],
                    })(
                        <Input
                            placeholder="School Number"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="normal" style={{margin: "0 3px"}} onClick={this.showModalCardReg}>
                        Register Card
                    </Button>
                    <Button type="primary" style={{margin: "0 3px"}} onClick={this.handleSubmit} disabled={false}>
                        Save Changes
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

class MemberEditFormComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            visibleCardMod: false,
            CardModNum: "",
            submitBtnStatus: true,
            MemberName: "",
            MemberSchNo: "",
            MemberClass: ""
        };
    }

    showModalCardMod = () => {
        this.setState({
            visibleCardMod: true
        });
    };

    detectCardModChange = (value) => {
        if (value.length >= 10) {
            this.setState({
                visibleCardMod: false,
                CardModNum: value,
                submitBtnStatus: false
            });
        }
    };

    handleOkCardMod = () => {
        this.setState({
            visibleCardMod: false
        });
    };

    handleCancelCardMod = () => {
        this.setState({
            visibleCardMod: false
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch('https://atndsys-server.now.sh/editmember?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW') + '&class=' + values.class + '&schno=' + values.schno + '&name=' + values.name + '&cardnum=' + this.state.CardModNum + '&target=' + this.props.targetMemberEditData.schno).then(response => response.text()).then(data => {
                    if (data === true || data === "true") {
                        message.info('Details modified successfully.');
                    }
                });
                this.props.form.resetFields();
                this.setState({editMode: false});
            }
        });
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.targetMemberEditData !== this.props.targetMemberEditData) {
            if (this.props.targetMemberEditData !== "") {
                this.setState({editMode: true});
                if (this.props.targetMemberEditData.cardnum === "undefined" || this.props.targetMemberEditData.cardnum === undefined) {
                    this.setState({CardModNum: ""});
                } else {
                    this.setState({CardModNum: this.props.targetMemberEditData.cardnum});
                }
                this.props.form.setFieldsValue({
                    'name': this.props.targetMemberEditData.name,
                    'schno': this.props.targetMemberEditData.schno,
                    'class': this.props.targetMemberEditData.class
                });
            } else {
                this.setState({editMode: false,CardModNum: ""});
                this.props.form.setFieldsValue({
                    'name': "",
                    'schno': "",
                    'class': ""
                });
            }
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Modal
                    title="Modify Card Details"
                    visible={this.state.visibleCardMod}
                    onOk={this.handleOkCardMod}
                    onCancel={this.handleCancelCardMod}
                    footer={[
                        <Button key="cancel" type="normal" onClick={this.handleCancelCardMod}>
                            Cancel
                        </Button>
                    ]}
                    className="card-reg-modal"
                >
                    <div className="card-reg-modal-icon">
                        <img src={cardlogo} alt="Register Card"/>
                    </div>
                    <div className="card-reg-modal-indi">
                        <Text strong={true}>Tap card on scanner.</Text>
                    </div>
                    <div className="card-reg-modal-input">
                        <input type="text" autoFocus={true} onChange={(value) => {
                            this.detectCardModChange(value.target.value);
                        }}/>
                    </div>
                </Modal>
                <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'The name of the member is required.' }]
                    })(
                        <Input
                            placeholder="Name"
                            disabled={!this.state.editMode}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('class', {
                        rules: [{ required: true, message: 'Invalid class.', min: 3, max: 4 }],
                    })(
                        <Input
                            placeholder="Class"
                            disabled={!this.state.editMode}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('schno', {
                        rules: [{ required: true, message: 'Invalid school number.', min: 5, max: 5 }],
                    })(
                        <Input
                            placeholder="School Number"
                            disabled={!this.state.editMode}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="normal" style={{margin: "0 3px"}} onClick={this.showModalCardMod} disabled={!this.state.editMode}>
                        Edit Card
                    </Button>
                    <Button type="primary" htmlType="submit" style={{margin: "0 3px"}} onClick={this.handleSubmit} disabled={!this.state.editMode}>
                        Save Changes
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

class ActivityAddFormComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startTime: "",
            endTime: "",
            date: "",
            big_event: false
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch('https://atndsys-server.now.sh/addactivity?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW') + '&date=' + this.state.date + '&name=' + values.name + '&starttime=' + this.state.startTime + '&endtime=' + this.state.endTime + '&venue=' + values.venue + '&big_event=' + this.state.big_event).then(response => response.text()).then(data => {
                    if (data === true || data === "true") {
                        message.info('New activity added successfully.');
                    }
                });
                this.props.form.resetFields();
            }
        });
    };

    format = 'HH:mm';

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'The name of the activity is required.' }],
                    })(
                        <Input
                            placeholder="Activity Name"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('venue', {
                        rules: [{ required: true, message: 'The venue of the activity is required.' }],
                    })(
                        <Input
                            placeholder="Activity Venue"
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('date')(
                        <DatePicker onChange={(data,value) => {
                            this.setState({date: value});
                        }}/>
                    )}
                </Form.Item>
                <Form.Item style={{display: 'inline-block'}}>
                    {getFieldDecorator('starttime')(
                        <TimePicker format={this.format} placeholder={'Start Time'} style={{marginRight: '0.8rem'}} onChange={(data, value) => {
                            this.setState({startTime: value});
                        }}/>
                    )}
                </Form.Item>
                <Form.Item style={{display: 'inline-block'}}>
                    {getFieldDecorator('endtime')(
                        <TimePicker format={this.format} placeholder={'Finish Time'} onChange={(data, value) => {
                            this.setState({endTime: value});
                        }}/>
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('big_event')(
                        <Checkbox defaultChecked={false} onChange={(e) => {this.setState({big_event: e.target.checked})}}>Big Event</Checkbox>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" onClick={this.handleSubmit}>
                        Save Changes
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

class ActivityEditFormComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editMode: false,
            ActName: "",
            ActVenue: "",
            ActDate: "",
            ActStartTime: "",
            ActEndTime: "",
            ActBigEvent: false,
            big_event: false
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.targetActivityEditData !== this.props.targetActivityEditData) {
            if (this.props.targetActivityEditData !== "") {
                this.setState({editMode: true});
                if (this.props.targetActivityEditData.big_event === "true") {
                    this.setState({big_event: true});
                } else {
                    this.setState({big_event: false});
                }
                this.setState({
                    ActStartTime: this.props.targetActivityEditData.starttime,
                    ActEndTime: this.props.targetActivityEditData.endtime,
                    ActDate: this.props.targetActivityEditData.date
                });
                this.props.form.setFieldsValue({
                    'name': this.props.targetActivityEditData.name,
                    'venue': this.props.targetActivityEditData.venue,
                    'date': moment(this.props.targetActivityEditData.date,'YYYY-MM-DD'),
                    'starttime': moment(this.props.targetActivityEditData.starttime,'HH:mm'),
                    'endtime': moment(this.props.targetActivityEditData.endtime,'HH:mm'),
                });
            } else {
                this.setState({editMode: false});
                this.props.form.resetFields();
            }
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch('https://atndsys-server.now.sh/editactivity?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW') + '&target=' + this.props.targetActivityEditData.id + '&date=' + this.state.ActDate + '&name=' + values.name + '&starttime=' + this.state.ActStartTime + '&endtime=' + this.state.ActEndTime + '&venue=' + values.venue + '&big_event=' + this.state.big_event).then(response => response.text()).then(data => {
                    if (data === true || data === "true") {
                        message.info('Activity modified successfully.');
                    }
                });
                this.props.form.resetFields();
            }
        });
    };

    format = 'HH:mm';

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'The name of the activity is required.' }],
                    })(
                        <Input
                            placeholder="Activity Name"
                            disabled={!this.state.editMode}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('venue', {
                        rules: [{ required: true, message: 'The venue of the activity is required.' }],
                    })(
                        <Input
                            placeholder="Activity Venue"
                            disabled={!this.state.editMode}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('date')(
                        <DatePicker disabled={!this.state.editMode} onChange={(data,value) => {
                            this.setState({ActDate: value});
                        }}/>
                    )}
                </Form.Item>
                <Form.Item style={{display: 'inline-block'}}>
                    {getFieldDecorator('starttime')(
                        <TimePicker format={this.format} placeholder={'Start Time'} style={{marginRight: '0.8rem'}} disabled={!this.state.editMode} onChange={(data, value) => {
                            this.setState({ActStartTime: value});
                        }}/>
                    )}
                </Form.Item>
                <Form.Item style={{display: 'inline-block'}}>
                    {getFieldDecorator('endtime')(
                        <TimePicker format={this.format} placeholder={'Finish Time'} disabled={!this.state.editMode} onChange={(data, value) => {
                            this.setState({ActEndTime: value});
                        }}/>
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('big_event')(
                        <Checkbox defaultChecked={false} checked={this.state.big_event} onChange={(e) => {this.setState({big_event: e.target.checked})}}>Big Event</Checkbox>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" onClick={this.handleSubmit} disabled={!this.state.editMode}>
                        Save Changes
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const MemberAddForm = Form.create()(MemberAddFormComp);
const MemberEditForm = Form.create()(MemberEditFormComp);
const ActivityAddForm = Form.create()(ActivityAddFormComp);
const ActivityEditForm = Form.create()(ActivityEditFormComp);

class Content extends Component {
    columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Class',
            dataIndex: 'class',
            key: 'class',
        },
        {
            title: 'Sch No.',
            dataIndex: 'schno',
            key: 'schno',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <span>
                    <p className="table-link" onClick={() => this.startMemberEdit(record)}>Edit</p>
                    <Divider type="vertical" />
                    <p className="table-link" onClick={() => this.showModalMemberDelete(record)}>Delete</p>
                </span>
            ),
        }
    ];

    columns1 = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Venue',
            dataIndex: 'venue',
            key: 'venue',
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: 'From',
            dataIndex: 'starttime',
            key: 'starttime',
        },
        {
            title: 'Till',
            dataIndex: 'endtime',
            key: 'endtime',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <span>
                    <p className="table-link" onClick={() => this.startActivityEdit(record)}>Edit</p>
                    <Divider type="vertical" />
                    <p className="table-link" onClick={() => this.showModalActivityDelete(record)}>Delete</p>
                </span>
            ),
        }
    ];

    columns2 = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Class',
            dataIndex: 'class',
            key: 'class',
        },
        {
            title: 'Sch No.',
            dataIndex: 'schno',
            key: 'schno',
        }
    ];

    columns3 = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Venue',
            dataIndex: 'venue',
            key: 'venue',
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: 'From',
            dataIndex: 'starttime',
            key: 'starttime',
        },
        {
            title: 'Till',
            dataIndex: 'endtime',
            key: 'endtime',
        }
    ];

    constructor(props) {
        super(props);
        this.state = {
            contentClass: "dashboard",
            loginRedirect: false,
            memberCount: "",
            activityCount: "",
            bigEventCount: "",
            upcomingAct: "",
            upcomingActElem: [],
            memberData: "",
            activityData: "",
            defaultTab: "1",
            targetEdit: "",
            loading: true,
            visibleMemberDelete: false,
            visibleActivityDelete: false,
            targetMemberDelete: "",
            targetActivityDelete: "",
            targetMemberEdit: "",
            memberEditTitle: "Edit Member Data",
            targetActivityEdit: ""
        };
    }

    showModalMemberDelete = (data) => {
        this.setState({
            visibleMemberDelete: true,
            targetMemberDelete: data
        });
    };

    showModalActivityDelete = (data) => {
        this.setState({
            visibleActivityDelete: true,
            targetActivityDelete: data
        });
    };

    startMemberEdit = (data) => {
        this.setState({
            defaultTab: "3",
            targetMemberEdit: data,
        });
    };

    startActivityEdit = (data) => {
        this.setState({
            defaultTab: "3",
            targetActivityEdit: data,
        });
    };

    handleOkMemberDelete = () => {
        this.setState({
            visibleMemberDelete: false,
        });
        fetch('https://atndsys-server.now.sh/deletemember?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW') + '&schno=' + this.state.targetMemberDelete.schno).then(response => response.text()).then(data => {
            this.setState({
                targetMemberDelete: "",
            });
            if (data === 'true' || data === true) {
                message.info('Member data has been deleted successfully.');
            }
        });
    };

    handleOkActivityDelete = () => {
        this.setState({
            visibleActivityDelete: false,
        });
        fetch('https://atndsys-server.now.sh/deleteactivity?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW') + '&actid=' + this.state.targetActivityDelete.id).then(response => response.text()).then(data => {
            this.setState({
                targetActivityDelete: "",
            });
            if (data === 'true' || data === true) {
                message.info('Activity data has been deleted successfully.');
            }
        });
    };

    handleCancelMemberDelete = () => {
        this.setState({
            visibleMemberDelete: false,
            targetMemberDelete: "",
        });
    };

    handleCancelActivityDelete = () => {
        this.setState({
            visibleActivityDelete: false,
            targetActivityDelete: "",
        });
    };

    handleTabChange = (selected) => {
        this.setState({
            defaultTab: selected,
            memberEditTitle: "Edit Member Data"
        });
    };

    handleTableChange = (pagination, filters, sorter) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
    };

    componentDidMount() {
        this.setState({memberData: ""});
        setInterval(() => {
            this.memberGetData();
            this.activityGetData();
        }, 3000);
        setTimeout(() => {
            this.setState({contentClass: "content fadein"});
        }, 500);
        document.title = "AtndSys " + this.props.title;
        if (this.props.page === "member" || this.props.page === "activity") {
            if (this.props.index) {
                this.setState({defaultTab: this.props.index.defaultTab});
            }
        }
        if (this.props.page === "activity") {
            if (this.props.index) {
                if (this.props.index.targetEdit) {
                    this.setState({targetEdit: this.props.index.targetEdit});
                }
            }
        }
        fetch('https://atndsys-server.now.sh/authtest?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW')).then(response => response.text()).then(data => {
            if (data !== "true") {
                this.setState({loginRedirect: true});
            } else {
                this.dashboardGetData();
                this.memberGetData();
                this.activityGetData();
            }
        });
    }

    showSkeletons = () => {
        this.setState({ loading: false });
    };

    dashboardGetData = () => {
        fetch('https://atndsys-server.now.sh/dashboarddataget?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW')).then(response => response.json()).then(data => {
            this.setState({memberCount: data.memberCount});
            this.setState({activityCount: data.activityCount});
            this.setState({bigEventCount: data.bigEventCount});
            this.setState({upcomingAct: data.upcomingAct});
            this.upcomingActAppend();
        });
    };

    memberGetData = () => {
        fetch('https://atndsys-server.now.sh/memberdataget?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW')).then(response => response.json()).then(data => {
            for (let i = 0; i <= data.length - 1; i++) {
                if (i === 0) {
                    memberdata = [];
                }
                data[i]["key"] = i + 1;
                memberdata.push(data[i]);
                this.setState({memberData : memberdata});
            }
        });
    };

    activityGetData = () => {
        fetch('https://atndsys-server.now.sh/activitydataget?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW')).then(response => response.json()).then(data => {
            for (let i = 0; i <= data.length - 1; i++) {
                if (i === 0) {
                    activitydata = [];
                }
                data[i]["key"] = i + 1;
                activitydata.push(data[i]);
                this.setState({activityData : activitydata});
                this.showSkeletons();
            }
        });
    };

    upcomingActAppend = () => {
        for (let i = 0; i <= this.state.upcomingAct.length - 1; i++) {
            let returningElem = this.state.upcomingActElem;
            returningElem.push(
                <div key={i}>
                    <Row className="activity-title">
                        <Title level={4}>{this.state.upcomingAct[i].name}</Title>
                    </Row>
                    <Row gutter={16} className="activity-details">
                        <Col>
                            <Statistic title="Activity Date" value={this.state.upcomingAct[i].date}/>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col>
                            <Statistic title="Activity Time" value={this.state.upcomingAct[i].starttime + " - " + this.state.upcomingAct[i].endtime}/>
                        </Col>
                    </Row>
                </div>
            );
            this.setState({upcomingActElem: returningElem});
        }
    };

    render() {
        let redirectElem;
        if (this.state.loginRedirect) {
            redirectElem = <Redirect to={'/'}/>
        }
        if (this.props.page === "dashboard") {
            return (
                <div className={this.state.contentClass}>
                    {redirectElem}
                    <Header title={this.props.title} index={this.props.page}/>
                    <div className="dashboard-sec">
                        <div className="dashboard-content">
                            <div className="dashboard-cardsec1 dashboard-cardsec">
                                <div className="dashboard-version dashboard-card">
                                    <Card title="AtndSys Version V2.0"
                                          extra={<a href="https://gitlab.com/icreatorz/atndsys">More</a>}
                                          style={{width: 300}}>
                                        <p>Changed how the system works and improved user interface.</p>
                                    </Card>
                                </div>
                                <div className="dashboard-date dashboard-card">
                                    <div style={{
                                        width: 300,
                                        border: '1px solid #d9d9d9',
                                        borderRadius: 4,
                                        background: "white"
                                    }}>
                                        <Calendar fullscreen={false}/>
                                    </div>
                                </div>
                            </div>
                            <div className="dashboard-cardsec2 dashboard-cardsec">
                                <div className="dashboard-member dashboard-card">
                                    <Card title="Member Overview" style={{width: 300}} actions={[
                                        <Tooltip title="Add Member"><Link to={{pathname: "/member", state: {defaultTab: "2"}}}><Icon type="plus"
                                                                                             key="plus"/></Link></Tooltip>,
                                        <Tooltip title="Edit Member"><Link to={{pathname: "/member", state: {defaultTab: "3"}}}><Icon type="edit"
                                                                                              key="edit"/></Link></Tooltip>,
                                        <Tooltip title="See All"><Link to={{pathname: "/member", state: {defaultTab: "1"}}}><Icon type="eye" key="eye"/></Link></Tooltip>,
                                    ]}>
                                        <Skeleton loading={this.state.loading} active>
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <Statistic title="Total Members" value={this.state.memberCount} prefix={<Icon type="user"/>}/>
                                                </Col>
                                                <Col span={12}>
                                                    <Statistic title="Active Members" value={this.state.memberCount} suffix={"/ " + this.state.memberCount}/>
                                                </Col>
                                            </Row>
                                            </Skeleton>
                                    </Card>
                                </div>
                                <div className="dashboard-member dashboard-card">
                                    <Card title="Inactive Members" style={{width: 300}}>
                                        <Row gutter={16} className="inactive-member">
                                            <Skeleton loading={this.state.loading} active>
                                                <Carousel autoplay>
                                                    <div>
                                                        <Title level={4}>-</Title>
                                                        <p> </p>
                                                    </div>
                                                </Carousel>
                                            </Skeleton>
                                        </Row>
                                    </Card>
                                </div>
                            </div>
                            <div className="dashboard-cardsec3 dashboard-cardsec">
                                <div className="dashboard-member dashboard-card">
                                    <Card title="Activity Overview" style={{width: 300}} actions={[
                                        <Tooltip title="Add Activity"><Link to={{pathname: "/activity", state: {defaultTab: "2"}}}><Icon type="plus"
                                                                                                 key="plus"/></Link></Tooltip>,
                                        <Tooltip title="Edit Activity"><Link to={{pathname: "/activity", state: {defaultTab: "3"}}}><Icon type="edit"
                                                                                                  key="edit"/></Link></Tooltip>,
                                        <Tooltip title="See All"><Link to={{pathname: "/activity", state: {defaultTab: "1"}}}><Icon type="eye"
                                                                                            key="eye"/></Link></Tooltip>,
                                    ]}>
                                        <Skeleton loading={this.state.loading} active>
                                            <Row gutter={16}>
                                                <Col span={12}>
                                                    <Statistic title="Total Activities" value={this.state.activityCount} prefix={<Icon type="calendar"/>}/>
                                                </Col>
                                                <Col span={12}>
                                                    <Statistic title="Big Events" value={this.state.bigEventCount} suffix={"/ " + this.state.activityCount}/>
                                                </Col>
                                            </Row>
                                        </Skeleton>
                                    </Card>
                                </div>
                                <div className="dashboard-member dashboard-card">
                                    <Card title="Upcoming Activity" style={{width: 300}} actions={[
                                        <Tooltip title="Edit Activity"><Link to={{pathname: "/activity", state: {defaultTab: "3",targetEdit: this.state.upcomingAct.name}}}><Icon type="edit" key="edit"/></Link></Tooltip>,
                                        <Tooltip title="See More"><Link to={{pathname: "/activity", state: {defaultTab: "1"}}}><Icon type="eye" key="eye"/></Link></Tooltip>,
                                    ]}>
                                        <Skeleton loading={this.state.loading} active>
                                            <Carousel autoplay dots={false}>
                                                {this.state.upcomingActElem}
                                            </Carousel>
                                        </Skeleton>
                                    </Card>
                                </div>
                            </div>
                            <div className="dashboard-cardsec4 dashboard-cardsec">
                                <div className="dashboard-contributors dashboard-card">
                                    <Card title="Contributors" style={{width: 300}}
                                          extra={<a href="https://gitlab.com/icreatorz/atndsys">More</a>}>
                                        <Row gutter={16}>
                                            <Col span={12}>
                                                <Statistic title="Developers" value={5}
                                                           prefix={<Icon type="user"/>}/>
                                            </Col>
                                            <Col span={12}>
                                                <Statistic title="Testers" value={1}
                                                           prefix={<Icon type="user"/>}/>
                                            </Col>
                                        </Row>
                                    </Card>
                                </div>
                                <div className="dashboard-contributors dashboard-card">
                                    <Card title="Server Status" style={{width: 300}} actions={[
                                        <Tooltip title="Report to Devs"><a
                                            href="https://gitlab.com/icreatorz/atndsys/issues"><Icon type="notification"
                                                                                                     key="notification"/></a></Tooltip>,
                                    ]}>
                                        <Row gutter={16}>
                                            <Col span={12}>
                                                <Statistic
                                                    title="Uptime"
                                                    value={0.00}
                                                    precision={2}
                                                    valueStyle={{color: '#3f8600'}}
                                                    prefix={<Icon type="arrow-up"/>}
                                                    suffix="%"
                                                />
                                            </Col>
                                            <Col span={12}>
                                                <Statistic
                                                    title="Downtime"
                                                    value={0.00}
                                                    precision={2}
                                                    valueStyle={{color: '#cf1322'}}
                                                    prefix={<Icon type="arrow-down"/>}
                                                    suffix="%"
                                                />
                                            </Col>
                                        </Row>
                                    </Card>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else if (this.props.page === "member") {
            return (
                <div className={this.state.contentClass}>
                    <Modal
                        title="Delete Confirmation"
                        visible={this.state.visibleMemberDelete}
                        onOk={this.handleOkMemberDelete}
                        onCancel={this.handleCancelMemberDelete}
                        footer={[
                            <Button type="normal" onClick={this.handleCancelMemberDelete}>
                                Cancel
                            </Button>,
                            <Button key="submit" type="danger" onClick={this.handleOkMemberDelete}>
                                Delete
                            </Button>
                        ]}
                    >
                        <p>You're going to delete {this.state.targetMemberDelete.name}'s data. <br/> This cannot be undone once performed.</p>
                    </Modal>
                    {redirectElem}
                    <Header title={this.props.title} index={this.props.page}/>
                    <div className="member-sec">
                        <div className="member-tabs">
                            <Tabs activeKey={this.state.defaultTab} onChange={this.handleTabChange}>
                                <TabPane tab={
                                    <span>
                                        <Icon type="user"/>
                                        All Members
                                    </span>
                                } key="1">
                                    <div className="member-tab-content-sec">
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Members" style={{width: 300}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Row gutter={16}>
                                                            <Col span={12}>
                                                                <Statistic title="Total Members" value={this.state.memberCount} prefix={<Icon type="user"/>}/>
                                                            </Col>
                                                            <Col span={12}>
                                                                <Statistic title="Active Members" value={this.state.memberCount} suffix={"/ " + this.state.memberCount}/>
                                                            </Col>
                                                        </Row>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Inactive Members" style={{width: 300}}>
                                                    <Row gutter={16} className="inactive-member">
                                                        <Skeleton loading={this.state.loading} active>
                                                            <Carousel autoplay>
                                                                <div>
                                                                    <Title level={4}>-</Title>
                                                                    <p> </p>
                                                                </div>
                                                            </Carousel>
                                                        </Skeleton>
                                                    </Row>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Member Datas" style={{width: 790}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Table columns={this.columns2} dataSource={this.state.memberData} pagination={{pageSize: 4}} onChange={this.handleTableChange}/>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tab={
                                    <span>
                                        <Icon type="plus"/>
                                        Add New Member
                                    </span>
                                } key="2">
                                    <div className="member-tab-content-sec">
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Member Datas" style={{width: 700}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Table columns={this.columns2} dataSource={this.state.memberData} pagination={{pageSize: 4}} onChange={this.handleTableChange}/>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Add New Member" style={{width: 380}}>
                                                    <MemberAddForm />
                                                </Card>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tab={
                                    <span>
                                        <Icon type="edit"/>
                                        Edit Member Data
                                    </span>
                                } key="3">
                                    <div className="member-tab-content-sec">
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Member Datas" style={{width: 700}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Table columns={this.columns} dataSource={this.state.memberData} pagination={{pageSize: 4}} onChange={this.handleTableChange}/>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title={this.state.memberEditTitle} style={{width: 380}}>
                                                    <MemberEditForm targetMemberEditData={this.state.targetMemberEdit}/>
                                                </Card>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            );
        } else if (this.props.page === "activity") {
            return (
                <div className={this.state.contentClass}>
                    <Modal
                        title="Delete Confirmation"
                        visible={this.state.visibleActivityDelete}
                        onOk={this.handleOkActivityDelete}
                        onCancel={this.handleCancelActivityDelete}
                        footer={[
                            <Button type="normal" onClick={this.handleCancelActivityDelete}>
                                Cancel
                            </Button>,
                            <Button key="submit" type="danger" onClick={this.handleOkActivityDelete}>
                                Delete
                            </Button>
                        ]}
                    >
                        <p>You're going to delete {this.state.targetActivityDelete.name}. <br/> This cannot be undone once performed.</p>
                    </Modal>
                    {redirectElem}
                    <Header title={this.props.title} index={this.props.page}/>
                    <div className="activity-sec">
                        <div className="activity-tabs">
                            <Tabs activeKey={this.state.defaultTab} onChange={this.handleTabChange}>
                                <TabPane tab={
                                    <span>
                                        <Icon type="calendar"/>
                                        All Activities
                                    </span>
                                } key="1">
                                    <div className="member-tab-content-sec">
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Activity Overview" style={{width: 300}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Row gutter={16}>
                                                            <Col span={12}>
                                                                <Statistic title="Total Activities" value={this.state.activityCount} prefix={<Icon type="calendar"/>}/>
                                                            </Col>
                                                            <Col span={12}>
                                                                <Statistic title="Big Events" value={this.state.bigEventCount} suffix={"/ " + this.state.activityCount}/>
                                                            </Col>
                                                        </Row>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Upcoming Activity" style={{width: 300}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Carousel autoplay dots={false}>
                                                            {this.state.upcomingActElem}
                                                        </Carousel>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Activity Datas" style={{width: 790}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Table columns={this.columns1} dataSource={this.state.activityData} pagination={{pageSize: 4}} onChange={this.handleTableChange}/>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tab={
                                    <span>
                                        <Icon type="plus"/>
                                        Add New Activity
                                    </span>
                                } key="2">
                                    <div className="member-tab-content-sec">
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Activity Datas" style={{width: 690}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Table columns={this.columns3} dataSource={this.state.activityData} pagination={{pageSize: 4}} onChange={this.handleTableChange}/>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Add New Activity" style={{width: 380}}>
                                                    <ActivityAddForm />
                                                </Card>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tab={
                                    <span>
                                        <Icon type="edit"/>
                                        Edit Activity Data
                                    </span>
                                } key="3">
                                    <div className="member-tab-content-sec">
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Activity Datas" style={{width: 690}}>
                                                    <Skeleton loading={this.state.loading} active>
                                                        <Table columns={this.columns1} dataSource={this.state.activityData} pagination={{pageSize: 4}} onChange={this.handleTableChange}/>
                                                    </Skeleton>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="dashboard-cardsec2 dashboard-cardsec">
                                            <div className="dashboard-member dashboard-card">
                                                <Card title="Edit Activity Data" style={{width: 380}}>
                                                    <ActivityEditForm targetActivityEditData={this.state.targetActivityEdit}/>
                                                </Card>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            );
        } else if (this.props.page === "mark") {
            return (
                <div className={this.state.contentClass}>
                    <Header title={this.props.title} index={this.props.page}/>
                </div>
            );
        } else {
            return (
                <div>
                    {redirectElem}
                    <Header title="" index={this.props.page}/>
                </div>
            );
        }
    }
}

export default Content;
