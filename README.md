# Project AtndSys (Attendance System)

AtndSys is a cloud-enabled, web-based attendance system for i-CreatorZ Club built with NodeJS and React.

## Contributors' Guide
---

#### Prerequisites
- Git
- Node with npm
- An Internet browser (webkit-based)
- A text editor or IDE
    
#### Installation
 ```sh
$ git clone https://gitlab.com/icreatorz/atndsys.git
$ cd atndsys
$ npm install
$ npm start
```

### Submitting Patches
Before submitting patches:
  - Make sure you're on a forked repo
  - Write a good commit message to tell us the changes made